from datetime import datetime
from dateutil.parser import parse as parsedt


def parse_time(time_str: str) -> datetime:
    """
    Convert time string to datetime, e.g. 9 AM to datetime(2020,1,1,9,0)
    """
    return parsedt(time_str)


def parse_date(date_str: str) -> datetime:
    """
    Convert date string to date, e.g., 9.2. to datetime(currentyear,2,9)
    """
    date_list = date_str.split(".")
    if date_list[-1]=="":
        date_list = date_list[0:-1]
    if len(date_list) == 1:
        return parsedt(date_str)
    if len(date_list) == 3:
        year = int(date_list[-1])
    else:
        year = datetime.now().year
    return datetime(year,int(date_list[1]),int(date_list[0]))

