# from __future__ import print_function
from datetime import datetime
from pytz import timezone
import pickle
import os.path
from googleapiclient.discovery import build


def get_gcp_service(token: str):
    """
    Return calendar service object
    https://developers.google.com/calendar/quickstart/python
    """
    if os.path.exists(token):
        with open(token, 'rb') as token:
            creds = pickle.load(token)
    else:
        raise Exception('Token {} not found'.format(token))
    return build('calendar', 'v3', credentials=creds)


def list_gcp_events(token: str, calendarId: str, nr_of_events: int, starting: datetime):
    """
    Get the list of upcomming events
    
    ## Input
    * token: token/authentification file (pkl). See create_gcp_token function
    * calendarId: calendar ID = in form of email address (see calendar settings)
    * nr_of_events: number of events to be listed 
    * starting: stargint time to count nr_of_events (string, use to_gcp_time for conversion). Set to None for current time

    ## Output
    * list of dictionaries, see https://developers.google.com/calendar/v3/reference/events for details 

    ## Example:
    events = list_gcp_events("calendar_tools/tests/_aux_/credentials_gcp.pkl",
                    "etfacktgqhn47qm4b5e4680ggk@group.calendar.google.com",
                    1,
                    "2020-2-9T12:00:00+01:00")
    """
    service = get_gcp_service(token)
    if starting is None:
        now = datetime.utcnow().isoformat() + 'Z'  # 'Z' indicates UTC time
    else:
        now = starting
    events_result = service.events().list(calendarId=calendarId,
                                          timeMin=now,
                                          maxResults=nr_of_events,
                                          singleEvents=True,
                                          orderBy='startTime').execute()
    return events_result.get('items', [])


def insert_gcp_event(token: str, calendarId: str,
                 start: str, stop: str, summary: str, 
                 description = None,
                 location = None):
    """
    Add event to google calendar. Returns ID of the created event.  
    
    ## Input
    * token: token/authentification file (pkl). See create_gcp_token function
    * calendarId: calendar ID = in form of email address (see calendar settings)
    * start: starting date-time string. Use to_gcp_time function for conversion 
    * stop: end date-time string. Use to_gcp_time function for conversion
    * summary: event name
    * description: optional event detail description
    * location: optional location string, e.g., Brno

    ## Output:
    * eventId. None if error occurs

    ## Example:
    ```
    event_id = insert_gcp_event("calendar_tools/tests/_aux_/credentials_gcp.pkl",
             "etfacktgqhn47qm4b5e4680ggk@group.calendar.google.com",
             '2020-1-25T12:00:00+02:00',
             '2020-1-25T13:00:00+02:00',
             'Initial test from python',
             description = 'Running test from python',
             location = 'Bratislava, Slovensko')
    ```
    """
    service = get_gcp_service(token)
    # https://developers.google.com/calendar/v3/reference/events/insert
    event = {
            'summary': summary,
            'start': {
                      'dateTime': start,
                    },
            'end': {
                    'dateTime': stop,
                   }
            }
    if not location is None:
        event["location"] = location
    if not description is None:
        event["description"] = description
    
    try:
        event = service.events().insert(calendarId=calendarId,
                                        body=event).execute()
        return event["id"]
    except Exception as e:
        return None


def get_gcp_event(token: str, calendarId: str, eventId: str) -> dict:
    """
    Get information about event using token pkl file, calendarId (in form of email address) and eventId
    
    ## Input
    * token: token/authentification file (pkl). See create_gcp_token function
    * calendarId: calendar ID = in form of email address (see calendar settings)
    * eventId: event unique ID (string)

    ## Output
    * either event dictionary or None if event does not exist or was cancelled or some other error occurs
    ## Example
    ```
    event = get_gcp_event("calendar_tools/tests/_aux_/credentials_gcp.pkl",
						  "etfacktgqhn47qm4b5e4680ggk@group.calendar.google.com",
						  "7f35nmmv59qvrg5greqh4e17oj")
    ```
    """
    service = get_gcp_service(token)
    try:
        event = service.events().get(calendarId=calendarId,
                                     eventId=eventId).execute()
        if event["status"]=="cancelled":
            return None
        else:
            return event
    except Exception as e:
        return None


def remove_gcp_event(token: str, calendarId: str, eventId: str) -> bool:
    """
    Delete event specified by unique ID
    https://developers.google.com/calendar/v3/reference/events/delete

    ## Input
    * token: token/authentification file (pkl). See create_gcp_token function
    * calendarId: calendar ID = in form of email address (see calendar settings)
    * eventId: event unique ID (string)

    ## Output
    * returns True for confirmation (False = not removed)

    ## Example:
    remove_event("d:/study/gcp/calendar/token_demo_restauracia.pkl",
                 "etfacktgqhn47qm4b5e4680ggk@group.calendar.google.com",
                 's9reafit640mcl7jujopd8irrs')
    """
    service = get_gcp_service(token)
    try:
        service.events().delete(calendarId=calendarId,
                                eventId=eventId).execute()
        return True
    except Exception as e:
        return False


def to_gcp_time(dt: datetime,
                tz: timezone = timezone('Europe/Bratislava')) -> str:
    """
    Format datetime to string used by Google (calendar) given the timezone (tz)
    https://stackoverflow.com/questions/11437414/google-calendar-api-time-format

    ## Example:
    ```
    from pytz import timezone
    from datetime import datetime
    dt = datetime(2020,1,26,9,30,0)
    out = to_gcp_time(dt,tz=timezone('Europe/Bratislava'))
    # check: either summer or winter time
    assert (out == '2020-01-26T09:30:00+01:00') or\
           (out == '2020-01-26T09:30:00+02:00')
    ```
    """
    fmt = '%Y-%m-%dT%H:%M:%S%z'
    loc_dt = tz.localize(dt)
    out_dt = loc_dt.strftime(fmt)
    # insert ":" to zone offset (by default +0100 instead of +01:00)
    return out_dt[0:22]+":"+out_dt[22:]


def from_gcp_time(dts: str) -> datetime:
    """
    Convert google time-formatted string to datetime (without time zone)

    ## Example
    ```
    assert from_gcp_time("2020-1-25T12:00:00-02:00") == datetime(2020,1,25,12,0,0)
    assert from_gcp_time("2020-12-10T11:20:33+01:00") == datetime(2020,12,10,11,20,33)
    ```
    """
    return datetime.strptime(dts[:-6], '%Y-%m-%dT%H:%M:%S')





