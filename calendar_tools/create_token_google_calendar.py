from __future__ import print_function
import pickle
from google_auth_oauthlib.flow import InstalledAppFlow


def create_gcp_token():
    """
    Get calendar token (will be stored in pickle) using creadentails + manual
    authentification
    For instructions, see readme  
    
    # Input
    * out_token: save token to this file, e.g., "token.pkl". Will overwirte if exists
    * credentials: json with OAuth credentials, see https://developers.google.com/calendar/quickstart/python
    * scopes: list of scopes, e.g., ['https://www.googleapis.com/auth/calendar.readonly']
    """
    # get user input
    # scalendar_name = input("set calendar name: ")
    out_token = input("output token file: ")
    credentials = input("credentials json file: ")
    scopes = input("scopes, e.g. https://www.googleapis.com/auth/calendar.events: ")
    flow = InstalledAppFlow.from_client_secrets_file(
        credentials,
        [scopes])
    # flow.user_agent = calendar_name
    creds = flow.run_local_server(port=0)
    # Save the credentials for the next run
    with open(out_token, 'wb') as token:
        pickle.dump(creds, token)


if __name__ == '__main__':
    create_gcp_token()

