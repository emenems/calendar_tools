from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
from email.header import Header
import smtplib
from datetime import datetime, timedelta
import random
import os


def compose_mail(from_mail: str, 
                 to_mail: list, 
                 subject: str, 
                 body: str, 
                 cc_mail: list = [],
                 from_name: str = None,
                 attachment: str = "",
                 content_type: str = "plain"):
    """
    Compose mail message and add attachment if required. Will not send the email!

    ## Input:
    * from_mail: send mail from this email address
    * to_mail: send mail to this list of recipients
    * subject: email subject
    * from_name: set name to be displayed as sender (email by default)
    * body: mail main message/body
    * attachment: optional file name of the attachment
    * content_type: plain (default) or html type of content

    ## Output:
    * MIME object (message). See send_mail_\* function for sending mails

    ## Example:
    ```
    from_mail = "PN-ROB-NES-PTP-DIG-One.DTSESK@external.telekom.com"
    to_mail = ["michal.mikolaj@telekom.com"]
    mail_message = compose_mail(from_mail,
                                to_mail,
                                "Test file_tools package",
                                "Hi,\nThis is a test of mail composition function.\n\nMichal")
    ```
    """
    message = MIMEMultipart("alternative")
    message["Subject"] = subject
    if from_name:
        message["From"] = str(Header(f"{from_name} <{from_mail}>"))
    else:
        message["From"] = from_mail
    message["To"] = ", ".join(to_mail)
    if cc_mail != []:
        message["CC"] = ", ".join(cc_mail)
    if attachment != "":
        attachment = [attachment] if type(attachment)==str else attachment
        part = MIMEBase('application', "octet-stream")
        for f in attachment:
            part = MIMEBase('application', "octet-stream")
            part.set_payload(open("{}".format(f), "rb").read())
            encoders.encode_base64(part)
            part.add_header('Content-Disposition', 'attachment; filename="{}"'.format(os.path.split(f)[-1]))
            message.attach(part)

    message.attach(MIMEText(body, content_type))
    return message


def send_mail(from_mail: str, 
              to_mail: list, 
              subject: str, body: str, 
              credentials: dict = None,
              cc_mail: list = [],
              from_name: str = None,
              attachment: str = "",
              host: str = "mailbb.t-systems.com",
              port: int = 25,
              content_type: str = "plain") -> None:
    """
    Send email using host and port for a given server

    ## Input:
    * from_mail: send mail from this email address
    * to_mail: send mail to this list of recipients
    * subject: email subject 
    * body: mail main message/body
    * from_name: set name to be displayed as sender (email by default/if set to None)
    * credentials: dictionary containing password and username
    * attachment: optional file name of the attachment
    * host: SMTP host
    * port: SMTP port
    * content_type: plain (default) or html type of content

    ## Example:
    ```
    from file_toolbox import send_mail_dtse
    from_mail = "PN-ROB-NES-PTP-DIG-One.DTSESK@external.telekom.com"
    to_mail = ["Michal.Mikolaj@telekom.com"]
    send_mail(from_mail,
              to_mail,
              "Test calendar_tools package",
              "Hi,\nThis is a test of mail composition function.\n\nMichal",
              attachment="file_toolbox/tests/_aux_/read_log.log")
    ```
    """
    message = compose_mail(from_mail,\
                           to_mail,\
                           subject,\
                           body,\
                           cc_mail = cc_mail,\
                           from_name=from_name,\
                           attachment=attachment,\
                           content_type=content_type)
    s = smtplib.SMTP(host=host, port=port)
    s.ehlo()
    s.starttls()
    if credentials:
        s.login(credentials["username"], credentials["password"])
    s.sendmail(from_mail, to_mail+cc_mail, message.as_string())


def format_event(file_ics: str, details: dict):
    """
    Format outlook event/invitation using 
    
    ## Input:
    * file_ics: file name = output
    * details: dictionary containing event details = organizer (email) start, end (datetime), description (body), summary (title)
    ## Example:
    ```
    from calendar_tools import format_event
    file_ics = "tests/_aux_/test_format_event.ics"
    details = {"start":datetime(2020,11,1,14,0,0),\
               "end":datetime(2020,11,1,14,30,0),\
               "organizer":"michal.mikolaj@telekom.com",\
               "description":"Formatting test.\\nHere goes the description + webex link\\nKind Regards...",\
               "summary":"Test Formatting",
               "status":"BUSY"}
    format_event(file_ics,details)


    file_ics = "C:/Users/A103421516/OneDrive - Deutsche Telekom AG/Desktop/test_format_event.ics"
    details = {"start":datetime(2021,9,14),\
               "end":datetime(2021,9,14),\
               "organizer":"michal.mikolaj@telekom.com",\
               "description":"Formatting test.\\nHere goes the description + webex link\\nKind Regards...",\
               "summary":"Test Formatting",
               "status":"FREE"}

    format_event(file_ics,details)
    ```
    """
    strftime = "%Y%m%dT%H%M%SZ"
    dtstamp = datetime.now().strftime(strftime)
    whole_day = (details["start"] == details["end"]) or \
                ((details["start"].hour==0 and details["start"].minute==0) and \
                (details["end"].hour==23 and details["end"].minute==59) and\
                (details["start"].month == details["end"].month and details["start"].day == details["end"].day)) 
                
    if "status" not in details.keys():
        details["status"] = "BUSY"
    with open(file_ics,"w") as fid:
        fid.write(f"BEGIN:VCALENDAR\n")
        fid.write(f"PRODID:-//Microsoft Corporation//Outlook 16.0 MIMEDIR//EN\n")
        fid.write(f"VERSION:2.0\n")
        #fid.write(f"METHOD:PUBLISH\n")
        fid.write(f"X-MS-OLK-FORCEINSPECTOROPEN:TRUE\n")
        fid.write(f"BEGIN:VTIMEZONE\n")
        fid.write(f"TZID:Central Europe Standard Time\n")
        fid.write(f"BEGIN:STANDARD\n")
        fid.write(f"DTSTART:16011028T030000\n")
        fid.write(f"RRULE:FREQ=YEARLY;BYDAY=-1SU;BYMONTH=10\n")
        fid.write(f"TZOFFSETFROM:+0200\n")
        fid.write(f"TZOFFSETTO:+0100\n")
        fid.write(f"END:STANDARD\n")
        fid.write(f"BEGIN:DAYLIGHT\n")
        fid.write(f"DTSTART:16010325T020000\n")
        fid.write(f"RRULE:FREQ=YEARLY;BYDAY=-1SU;BYMONTH=3\n")
        fid.write(f"TZOFFSETFROM:+0100\n")
        fid.write(f"TZOFFSETTO:+0200\n")
        fid.write(f"END:DAYLIGHT\n")
        fid.write(f"END:VTIMEZONE\n")
        fid.write(f"BEGIN:VEVENT\n")
        fid.write(f"CLASS:PUBLIC\n")
        fid.write(f"CREATED:{dtstamp}\n")
        fid.write(f"DESCRIPTION:{details['description']}\n")
        if whole_day == False:
            fid.write(f"DTEND;TZID=\"Central Europe Standard Time\":{details['end'].strftime(strftime[0:-1])}\n")
        else:
            fid.write(f"DTEND;TZID=\"Central Europe Standard Time\":{(details['start']+timedelta(days=1)).strftime(strftime.split('T')[0])}\n")
        fid.write(f"DTSTAMP:{dtstamp}\n")
        if whole_day == False:
            fid.write(f"DTSTART;TZID=\"Central Europe Standard Time\":{details['start'].strftime(strftime[0:-1])}\n")
        else:
            fid.write(f"DTSTART;VALUE=DATE:{details['start'].strftime(strftime.split('T')[0])}\n")
        fid.write(f"LOCATION:\n")
        fid.write(f"SUMMARY:{details['summary']}\n")
        fid.write(f"TRANSP:OPAQUE\n")
        fid.write(f"UID:{dtstamp}-{random.getrandbits(64)}-{details['organizer']}\n")
        fid.write(f"X-MICROSOFT-CDO-BUSYSTATUS:{details['status'].upper()}\n")
        fid.write(f"X-MICROSOFT-CDO-IMPORTANCE:1\n")
        fid.write(f"X-MICROSOFT-CDO-INTENDEDSTATUS:{details['status'].upper()}\n")
        fid.write(f"X-MICROSOFT-DISALLOW-COUNTER:FALSE\n")
        #fid.write(f"X-MS-OLK-APPTSEQTIME:20200904T082859Z\n")
        fid.write(f"BEGIN:VALARM\n")
        fid.write(f"TRIGGER:-PT15M\n")
        fid.write(f"ACTION:DISPLAY\n")
        fid.write(f"DESCRIPTION:Reminder\n")
        fid.write(f"END:VALARM\n")
        fid.write(f"END:VEVENT\n")
        fid.write(f"END:VCALENDAR\n")
