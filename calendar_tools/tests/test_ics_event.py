from datetime import datetime
from pytz import timezone
from calendar_tools import ics_event
import re
import os


def test_format_event():
    file_ics = "calendar_tools/tests/_aux_/test_format_event_out.ics"
    details = {"start":datetime(2020,11,1,14,0,0),\
               "end":datetime(2020,11,1,14,30,0),\
               "organizer":"michal.mikolaj@telekom.com",\
               "description":"Formatting test.\\nHere goes the description + webex link\\nKind Regards...",\
               "summary":"Test Formatting"}
    ics_event.format_event(file_ics,details)
    with open(file_ics,"r") as fid:
        event = fid.read()
    strftime = "%Y%m%dT%H%M%S"
    assert len(re.findall(r"CREATED:[0-9]{8}T[0-9]{6}Z",event))==1
    assert f"DESCRIPTION:{details['description']}" in event
    assert f"SUMMARY:{details['summary']}" in event
    assert f"DTEND;TZID=\"Central Europe Standard Time\":{details['end'].strftime(strftime)}" in event
    assert f"DTSTART;TZID=\"Central Europe Standard Time\":{details['start'].strftime(strftime)}" in event
    assert details['organizer'] in event
    assert "BUSYSTATUS:BUSY" in event

    details["status"] = "FREE"
    details["start"] = datetime(2021,9,1)
    details["end"] = datetime(2021,9,1)
    ics_event.format_event(file_ics,details)
    with open(file_ics,"r") as fid:
        event = fid.read()
    assert "BUSYSTATUS:FREE" in event
    assert "DTSTART;VALUE=DATE:" in event
    os.remove(file_ics)
    

def test_compose_mail():
    from_mail = "PN-ROB-NES-PTP-DIG-One.DTSESK@external.telekom.com"
    to_mail = ["test@telekom.com"]
    cc_mail = "test_cc@telekom.com"
    attachment = ["calendar_tools/tests/_aux_/attachment_test.txt"]
    mail_message = ics_event.compose_mail(from_mail,
                                         to_mail,
                                         "Test calendar_tools package 1",
                                         "Hi,\nThis is a test of mail composition function.\n\nMichal",
                                         cc_mail=cc_mail,
                                         attachment=attachment)
    assert not mail_message is None
    
    mail_message = ics_event.compose_mail(from_mail,
                                         "test@telekom.com",
                                         "Test calendar_tools package 2",
                                         "Hi,<br>This is a test of mail composition function.<br><br>Michal",
                                         content_type="html")
    assert not mail_message is None
