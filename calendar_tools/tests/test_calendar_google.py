from datetime import datetime
from pytz import timezone
from calendar_tools import calendar_google
import os


CREDENTIALS_GCP = "calendar_tools/tests/_aux_/credentials_gcp.pkl"
CALENDAR_ID_GCP = "etfacktgqhn47qm4b5e4680ggk@group.calendar.google.com"


def test_to_gcp_time():
	dt = datetime(2020,1,26,9,30,0)
	out = calendar_google.to_gcp_time(dt)
	assert (out == '2020-01-26T09:30:00+01:00') or (out == '2020-01-26T09:30:00+02:00')
	out = calendar_google.to_gcp_time(datetime(1999,2,3,12,29,28),
									  tz = timezone('Europe/London'))
	assert out == '1999-02-03T12:29:28+00:00'


def test_from_gcp_time():
	out = calendar_google.from_gcp_time("2020-1-25T12:00:00-02:00")
	assert out == datetime(2020,1,25,12,0,0)
	out = calendar_google.from_gcp_time("2020-12-10T11:20:33+01:00")
	assert out == datetime(2020,12,10,11,20,33)


def test_list_gcp_events():
	if os.path.isfile(CREDENTIALS_GCP):
		now = datetime(2020,2,9,16,0,0)
		events = calendar_google.list_gcp_events(CREDENTIALS_GCP,
												 CALENDAR_ID_GCP,
												 1,
												 calendar_google.to_gcp_time(now))
		assert len(events)==1
		events = calendar_google.list_gcp_events(CREDENTIALS_GCP,
												 CALENDAR_ID_GCP,
												 2,
												 calendar_google.to_gcp_time(now))
		assert len(events)==2
		assert events[0]["summary"] == "test_list_events1"
		assert events[0]["description"] == "List description 1"
		assert events[1]["description"] == "List description 2"
		assert events[0]["location"] == "Brno, Česko"
		assert events[0]["start"]["dateTime"] == "2020-02-09T16:30:00+01:00"
		assert events[0]["end"]["dateTime"] == "2020-02-09T17:00:00+01:00"
		assert events[1]["start"]["dateTime"] == "2020-02-10T10:00:00+01:00"
		assert events[1]["end"]["dateTime"] == "2020-02-10T11:00:00+01:00"


def test_get_gcp_event():
	if os.path.isfile(CREDENTIALS_GCP):
		event = calendar_google.get_gcp_event(CREDENTIALS_GCP,
							  				  CALENDAR_ID_GCP,
							  				  "7f35nmmv59qvrg5greqh4e17oj")
		assert event["description"] == "List description 1"
		assert event["location"] == "Brno, Česko"
		assert event["start"]["dateTime"] == "2020-02-09T16:30:00+01:00"
		assert event["end"]["dateTime"] == "2020-02-09T17:00:00+01:00"
		

def test_insert_and_remove_gcp_event():
	if os.path.isfile(CREDENTIALS_GCP):
		# create
		event_id = calendar_google.insert_gcp_event(CREDENTIALS_GCP,
             										CALENDAR_ID_GCP,
             										'2020-02-08T11:00:00+01:00',
             										'2020-02-08T12:00:00+01:00',
             										'test_insert_gcp_event',
             										description = 'Running unit test',
													location = 'Brno')
		# get info via API
		event = calendar_google.get_gcp_event(CREDENTIALS_GCP,
							  				  CALENDAR_ID_GCP,
							  				  event_id)
		# check
		assert event["summary"] == "test_insert_gcp_event"
		assert event["description"] == "Running unit test"
		assert event["location"] == "Brno"
		assert event["start"]["dateTime"] == "2020-02-08T11:00:00+01:00"
		assert event["end"]["dateTime"] == "2020-02-08T12:00:00+01:00"
		# remove
		removed = calendar_google.remove_gcp_event(CREDENTIALS_GCP,
								   				   CALENDAR_ID_GCP,
								   				   event_id)
		assert removed == True
		# try to get info again (should be empty because deleted)
		event_deleted = calendar_google.get_gcp_event(CREDENTIALS_GCP,
							  						  CALENDAR_ID_GCP,
							  						  event_id)
		assert event_deleted is None
		# try to remove already deleted event (shoud return False)

