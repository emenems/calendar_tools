from datetime import datetime
from calendar_tools import parse_datetime


def test_parse_date():
    year = datetime.now().year
    assert parse_datetime.parse_date("1.1.") == datetime(year,1,1)
    assert parse_datetime.parse_date("1.1") == datetime(year,1,1)
    assert parse_datetime.parse_date("12.1") == datetime(year,1,12)
    assert parse_datetime.parse_date("01.12.2019") == datetime(2019,12,1)
    assert parse_datetime.parse_date("01.12.2019.") == datetime(2019,12,1)
    assert parse_datetime.parse_date("1st December") == datetime(year,12,1)
    assert parse_datetime.parse_date("1st December 2019") == datetime(2019,12,1)
    assert parse_datetime.parse_date("2 february 2019") == datetime(2019,2,2)
    temp = parse_datetime.parse_date("2020-03-13T12:00:00+01:00")
    assert  temp.year == 2020 and temp.month == 3 and temp.day == 13


def test_parse_time():
    temp = datetime.now()
    assert parse_datetime.parse_time("9 AM") == datetime(temp.year,temp.month,temp.day,9,0)
    assert parse_datetime.parse_time("9 PM") == datetime(temp.year,temp.month,temp.day,21,0)
    assert parse_datetime.parse_time("9:00") == datetime(temp.year,temp.month,temp.day,9,0)
    assert parse_datetime.parse_time("12:00") == datetime(temp.year,temp.month,temp.day,12,0)
    assert parse_datetime.parse_time("21:30") == datetime(temp.year,temp.month,temp.day,21,30)