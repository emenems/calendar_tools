# Calendar toolbox
Set of functions to interact with calendars e.g., Google Calendar/Google Cloud Platform (GCP)

## Functions
* **get_gcp_event**: [fetch](https://developers.google.com/calendar/v3/reference/events/get) google calendar (GCP) even
* **list_gcp_events**: [list](https://developers.google.com/calendar/v3/reference/events/list) google calendar (GCP) events
* **insert_gcp_event**: [insert](https://developers.google.com/calendar/v3/reference/events/insert) google calendar (GCP) event
* **remove_gcp_events**: [remove](https://developers.google.com/calendar/v3/reference/events/delete) google calendar (GCP) event
* **from_gcp_time**: convert google calendar (GCP) to datetime
* **to_gcp_time**: convert datetime to google calendar (GCP) string
* **parse_time**: parse string (EU-formatted) time
* **parse_date**: parse string (EU-formatted) date
* **create_gcp_token**: create token (verification) for access to google calendar (GCP)
* **format_event**: format calendar event (create ics) file
* **compose_email**: prepare email bodu including attachment (like ics event)
* **send_mail**: use the compose_email and send mail passing only important parameters

### Installation
* Optional: create python [virtual environment](https://virtualenv.pypa.io/en/latest/userguide.html)
	* `virtualenv SOME-DIRECTORY`
	* `source /path/to/SOME-DIRECTORY/bin/activate`
	* to stop env simpy call `deactivate`
* install dependencies:
	* go to folder where test_package is located (not to env SOME-DIRECTORY)
	* `pip install -r requirements.txt`
* install package locally (for current user)
	* go to folder where test_package is located (where setup.py file is located)
	* `pip install --user .`

### Google Calendar OAuth key/token
* Follow steps: https://developers.google.com/calendar/auth or https://developers.google.com/identity/protocols/OAuth2
* Navigation Menu > API > Library > Calendar API > Enable
* Go to API > OAuth Consent > External > Create > Select Scope calendar.events > Save
* Go to API Services> Credentials > Create new > OAuth Client > Other > Set name, e.g., "google_calendar" > Create > Click on the download Icon
* run ```python calendar_tools\create_token_google_calendar.py```

### Test
* Before running test, make sure valid credentials are stored in `calendar_tools/tests/_aux_/calendar_*.pkl`
* To carry out [unit-test](https://docs.pytest.org/en/latest/), just go to the root folder (where setup.py is located) and run `pytest` from shell


### Usage
Library  
```python
# Import functions from packge
from calendar_tools import parse_time, to_gcp_time

# call parse_time function
parse_time("9:20")

# Get function help
help(to_gcp_time)
```

Token creation: run from shell  
```python calendar_tools\create_token_google_calendar.py```
