from setuptools import setup, find_packages

setup(name = 'calendar-tools',
      version = '0.1',
      description = 'Own calendar package',
      author = 'Michal Mikolaj',
      packages = find_packages(),
	zip_safe = False
     )